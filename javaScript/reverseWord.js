// 1. Take a sentence as an input and reverse every word in that sentence.
// Example - This is a sunny day > shiT si a ynnus yad

let str = "This is a sunny day";
const newStr = str.split(" ");
const ans = [];

for (const word of newStr) {
  const reversedStr = word.split("").reverse().join("");
  ans.push(reversedStr);
}

console.log(ans.join(" "));
