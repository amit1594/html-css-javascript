// 2. Enter a Roman Number as input and convert it to an integer. (Example: IX = 9)

import java.util.HashMap;

class HelloWorld {
    public static void main(String[] args) {
        String str= "IX";
        int n = str.length();
        HashMap<Character,Integer> mp = new HashMap<>();
        mp.put('I',1);
        mp.put('V',5);
        mp.put('X',10);
        mp.put('L',50);
        mp.put('C',100);
        mp.put('D',500);
        mp.put('M',1000);
        int val = 0;
        int prevValue = 0;
        int flag = 0;
        for (int i=0; i<str.length(); i++)   
        {   
          int s1 = mp.get(str.charAt(i));  
          if (i+1 <str.length())   
          {   
            int s2 = mp.get(str.charAt(i+1));
            if (s1 >= s2)  
                val += s1;   
            else  
                val -= s1; 
          }   
          else  
          {   
            val += s1;   
          }   
        }   
        System.out.print(val);
    }
}