// 3. Check if the input is pangram or not. (A pangram is a sentence that contains all the 
// alphabets from A to Z)

import java.util.HashSet;

class HelloWorld {
    public static void main(String[] args) {
        String s = "zabcdefghijklmnopqrstuvwxyz";
        s = s.toLowerCase();
        HashSet<Character> hs = new HashSet<>();
        for(int i=0;i<s.length();i++)
        {
            hs.add(s.charAt(i));
        }
        if(hs.size()==26)
        {
            System.out.print("Sentence is pangram");
        }
        else
        {
            System.out.print("Sentence is not a pangram");
        }
    }
}
